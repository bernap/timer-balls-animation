//
//  ContentView.swift
//  TryAnimation
//
//  Created by Gerel Burgustina on 29/03/2020.
//  Copyright © 2020 Gerel Burgustina. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var position: CGSize = CGSize(width: -120, height: -400)
    @State private var position1: CGSize = CGSize(width: -120, height: -500)
    @State private var position2: CGSize = CGSize(width: -120, height: -600)
    @State private var position3: CGSize = CGSize(width: -120, height: -700)
    @State private var position4: CGSize = CGSize(width: -120, height: -800)
    @State private var position5: CGSize = CGSize(width: -120, height: -900)
    @State private var position6: CGSize = CGSize(width: -120, height: -1000)
    @State private var position7: CGSize = CGSize(width: -120, height: -1200)
    @State private var position8: CGSize = CGSize(width: -120, height: -1300)
    @State private var position9: CGSize = CGSize(width: -120, height: -1400)
    
    
    var hours = Array(0...23)
    var minutes = Array(0...59)
    var seconds = Array(0...59)
    
    @State private var timer: Timer?
    @State private var selectedHours = 0
    @State private var selectedMins = 0
    @State private var selectedSecs = 0
    @State private var showButton = false
    @State private var showPicker = false
    @State private var totalSecsAtStart = 0
    @State private var durationFactor = 0.0

    
    
    
    
    
    func calculate() -> Int {
        let totalsecs = self.selectedSecs + self.selectedMins*60 + self.selectedHours*3600
        return totalsecs
    }
    
    var body: some View {
        VStack {
            ZStack {
                GeometryReader { geometry in
                    HStack(spacing: 0) {
                        Picker(selection: self.$selectedHours, label: Text("Hours")) {
                            ForEach(0..<self.hours.count) {
                                Text("\(self.hours[$0]) hours")
                            }
                        }
                            
                        .frame(maxWidth: geometry.size.width / 3)
                        .clipped()
                        .opacity(self.showPicker ? 0 : 1)
                        
                        Picker(selection: self.$selectedMins, label: Text("Mins")) {
                            ForEach(0..<self.minutes.count) {
                                Text("\(self.minutes[$0]) mins")
                            }
                        }
                            
                        .frame(maxWidth: geometry.size.width / 3)
                        .clipped()
                        .opacity(self.showPicker ? 0 : 1)
                        
                        Picker(selection: self.$selectedSecs, label: Text("Secs")) {
                            ForEach(0..<self.seconds.count) {
                                Text("\(self.seconds[$0]) secs")
                            }
                        }
                            
                        .frame(maxWidth: geometry.size.width / 3)
                        .clipped()
                        .opacity(self.showPicker ? 0 : 1)
                        

                    }
                    
                }
                VStack {
                    HStack {
                        
                        ballsView(color: .blue, animation: .easeInOut(duration: 5.3 * durationFactor), position: $position)
                        ballsView(color: .blue, animation: .easeIn(duration: 5.5 * durationFactor), position: $position)
                        ballsView(color: .black, animation: .easeInOut(duration: 3.6 * durationFactor), position: $position)
                        ballsView(color: .blue, animation: .easeOut(duration: 5.2 * durationFactor), position: $position)
                        //                ballsView(color: .blue, animation: .easeIn(duration: 1.1), position: $position)
                    }
                    
                    HStack {
                        ballsView(color: .blue, animation: .easeOut(duration: 4.8 * durationFactor), position: $position1)
                        ballsView(color: .blue, animation: .easeIn(duration: 5.1 * durationFactor), position: $position1)
                        ballsView(color: .black, animation: .easeInOut(duration: 4.9 * durationFactor), position: $position1)
                        ballsView(color: .blue, animation: .easeOut(duration: 5.0 * durationFactor), position: $position1)
                        ballsView(color: .blue, animation: .easeIn(duration: 4.8 * durationFactor), position: $position1)
                        //                ballsView(color: .blue, animation: .easeInOut(duration: 1.0), position: $position1)
                        
                    }
                    
                    HStack {
                        
                        ballsView(color: .blue, animation: .easeInOut(duration: 4.5 * durationFactor), position: $position2)
                        ballsView(color: .blue, animation: .easeOut(duration: 4.3 * durationFactor), position: $position2)
                        ballsView(color: .black, animation: .easeInOut(duration: 4.7 * durationFactor), position: $position2)
                        ballsView(color: .blue, animation: .easeIn(duration: 4.6 * durationFactor), position: $position2)
                        //                ballsView(color: .blue, animation: .easeIn(duration: 1.1), position: $position2)
                    }
                    
                    HStack {
                        ballsView(color: .blue, animation: .easeIn(duration: 3.9 * durationFactor), position: $position3)
                        ballsView(color: .blue, animation: .easeOut(duration: 4.1 * durationFactor), position: $position3)
                        ballsView(color: .black, animation: .easeInOut(duration: 4.0 * durationFactor), position: $position3)
                        ballsView(color: .blue, animation: .easeOut(duration: 4.2 * durationFactor), position: $position3)
                        ballsView(color: .blue, animation:
                            .easeIn(duration: 3.9 * durationFactor), position: $position3)
                        //                ballsView(color: .blue, animation: .easeInOut(duration: 1.0), position: $position3)
                    }
                    
                    HStack {
                        
                        ballsView(color: .blue, animation: .easeOut(duration: 3.6 * durationFactor), position: $position4)
                        ballsView(color: .blue, animation: .easeIn(duration: 3.4 * durationFactor), position: $position4)
                        ballsView(color: .black, animation: .easeInOut(duration: 3.7 * durationFactor), position: $position4)
                        ballsView(color: .blue, animation: .easeOut(duration: 3.8 * durationFactor), position: $position4)
                        //                ballsView(color: .blue, animation: .easeIn(duration: 1.1), position: $position4)
                    }
                    
                    HStack {
                        ballsView(color: .blue, animation: .easeInOut(duration: 3.0 * durationFactor), position: $position5)
                        ballsView(color: .blue, animation: .easeIn(duration: 3.2 * durationFactor), position: $position5)
                        ballsView(color: .black, animation: .easeOut(duration: 3.7 * durationFactor), position: $position5)
                        ballsView(color: .blue, animation: .easeInOut(duration: 3.5 * durationFactor), position: $position5)
                        ballsView(color: .blue, animation: .easeIn(duration: 3.3 * durationFactor), position: $position5)
                        //                    ballsView(color: .blue, animation: .easeInOut(duration: 1.0), position: $position5)
                        
                    }
                    
                    HStack {
                        
                        ballsView(color: .blue, animation: .easeOut(duration: 2.7 * durationFactor), position: $position6)
                        ballsView(color: .blue, animation: .easeIn(duration: 2.9 * durationFactor), position: $position6)
                        ballsView(color: .black, animation: .easeIn(duration: 3.0 * durationFactor), position: $position6)
                        ballsView(color: .blue, animation: .easeInOut(duration: 3.1 * durationFactor), position: $position6)
                        //                    ballsView(color: .blue, animation: .easeIn(duration: 1.1), position: $position6)
                    }
                    
                    HStack {
                        ballsView(color: .blue, animation: .easeInOut(duration: 2.6 * durationFactor), position: $position7)
                        ballsView(color: .blue, animation: .easeOut(duration: 2.9 * durationFactor), position: $position7)
                        ballsView(color: .black, animation: .easeInOut(duration: 2.7 * durationFactor), position: $position7)
                        ballsView(color: .blue, animation: .easeInOut(duration: 2.6 * durationFactor), position: $position7)
                        ballsView(color: .blue, animation: .easeIn(duration: 2.4 * durationFactor), position: $position7)
                        //                    ballsView(color: .blue, animation: .easeInOut(duration: 1.0), position: $position7)
                        
                    }
                    
                    HStack {
                        
                        ballsView(color: .blue, animation: .easeOut(duration: 2.5 * durationFactor), position: $position8)
                        ballsView(color: .blue, animation: .easeIn(duration: 2.4 * durationFactor), position: $position8)
                        ballsView(color: .black, animation: .easeInOut(duration: 2.3 * durationFactor), position: $position8)
                        ballsView(color: .blue, animation: .easeOut(duration: 2.3 * durationFactor), position: $position8)
                        //                    ballsView(color: .blue, animation: .easeIn(duration: 1.1), position: $position8)
                    }
                    
                    HStack {
                        ballsView(color: .blue, animation: .easeIn(duration: 2.0 * durationFactor), position: $position9)
                        ballsView(color: .blue, animation: .easeInOut(duration: 2.3 * durationFactor), position: $position9)
                        ballsView(color: .black, animation: .easeInOut(duration: 2.2 * durationFactor), position: $position9)
                        ballsView(color: .blue, animation: .easeIn(duration: 2.0 * durationFactor), position: $position9)
                        ballsView(color: .blue, animation: .easeOut(duration: 2.1 * durationFactor), position: $position9)
                        //                    ballsView(color: .blue, animation: .easeInOut(duration: 1.0), position: $position9)
                        
                    }
                }
            }
            
            
            HStack{
                Text("\(calculate()/3600,specifier: "%02d") :")
                    .font(.largeTitle)
                
                Text("\((calculate() / 60) % 60,specifier: "%02d") :")
                    .font(.largeTitle)
                
                Text("\(calculate() % 60,specifier: "%02d")")
                    .font(.largeTitle)
            }
            
            HStack {
                ZStack {
                    Button(action: {
                        self.totalSecsAtStart = self.calculate()
                        self.durationFactor = Double(self.totalSecsAtStart) / 5.5
                        self.position = CGSize(width: 1, height: 200)
                        self.position1 = CGSize(width: 1, height: 180)
                        self.position2 = CGSize(width: 1, height: 160)
                        self.position3 = CGSize(width: 1, height: 140)
                        self.position4 = CGSize(width: 1, height: 120)
                        self.position5 = CGSize(width: 1, height: 100)
                        self.position6 = CGSize(width: 1, height: 80)
                        self.position7 = CGSize(width: 1, height: 60)
                        self.position8 = CGSize(width: 1, height: 40)
                        self.position9 = CGSize(width: 1, height: 20)
                        
                        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                            self.selectedSecs -= 1
                            if self.calculate() <= 0 {
                                self.timer?.invalidate()
                                self.selectedSecs = 0
                                self.selectedMins = 0
                                self.selectedHours = 0
                            }
                        }
                        
                        withAnimation(){
                            self.showButton = true
                            self.showPicker = true
                            
                        }
                    }) {
                        Text("Start")
                            .font(.headline)
                    }
                    .opacity(showButton ? 0 : 1)
                    
                    Button(action: {
                        self.timer?.invalidate()
                        self.showPicker = true

                    
                        self.durationFactor = 0.5
                        
                        
                         
                        self.position = CGSize(width: 0, height: 50)
                        self.position1 = CGSize(width: 0, height: -50)
                        self.position2 = CGSize(width: 0, height: -50)
                        self.position3 = CGSize(width: 0, height: -150)
                        self.position4 = CGSize(width: 0, height: -250)
                        self.position5 = CGSize(width: 0, height: -350)
                        self.position6 = CGSize(width: 0, height: -450)
                        self.position7 = CGSize(width: 0, height: -650)
                        self.position8 = CGSize(width: 0, height: -750)
                        self.position9 = CGSize(width: 0, height: -850)
                       
                        withAnimation(){
                            self.showButton = false}
                    }) {
                        Text("Stop")
                            .font(.headline)
                    }
                    .opacity(showButton ? 1 : 0)
                }
                Button(action: {
                    self.timer?.invalidate()
                    //                    withAnimation(){
                    self.selectedHours = 0
                    self.selectedMins = 0
                    self.selectedSecs = 0
                    self.showButton = false
                    self.showPicker = false
                   
                    withAnimation() {
                    self.position = CGSize(width: -120, height: -400)
                    self.position1 = CGSize(width: -120, height: -500)
                    self.position2 = CGSize(width: -120, height: -600)
                    self.position3 = CGSize(width: -120, height: -700)
                    self.position4 = CGSize(width: -120, height: -800)
                    self.position5 = CGSize(width: -120, height: -900)
                    self.position6 = CGSize(width: -120, height: -1000)
                    self.position7 = CGSize(width: -120, height: -1200)
                    self.position8 = CGSize(width: -120, height: -1300)
                    self.position9 = CGSize(width: -120, height: -1400)
                                        }
                }) {
                    Text("Reset")
                        .font(.headline)
                }
            }
            Spacer(minLength: 100)
        }
    }
    
    
    struct ballsView: View {
        let color: Color
        let animation: Animation
        @Binding var position: CGSize
        @State private var gradientColor: [Color] = [.white, .black]
        @State private var startRadius: CGFloat = 30
        @State private var endradius: CGFloat = 600
        
        
        
        var body: some View {
            
            ZStack {
                Circle()
                    .fill(RadialGradient(gradient: Gradient(colors: gradientColor), center: .topLeading, startRadius: startRadius, endRadius: endradius))
                    .frame(width: 73, height: 73)
                    .shadow(color: .secondary, radius: 7, x: 0, y: 0)
                    .offset(position)
                    .animation(animation .repeatCount(1, autoreverses: true).speed(1))
                    
                    //            .animation(animation.repeatForever(autoreverses: true).speed(0.3))
                    .padding(1)
                    .onAppear() {
                        self.startRadius = CGFloat(Int(self.startRadius) % 170)
                        self.endradius = CGFloat(Int(self.endradius) % 350)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

